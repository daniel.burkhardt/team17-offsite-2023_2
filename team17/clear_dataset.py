import csv
import datetime
import argparse


def clear_csv(input_file_path, output_file_path):
    with open(
        input_file_path, "r", newline="", encoding="utf-8"
    ) as input_csvfile, open(
        output_file_path, "w", newline="", encoding="utf-8"
    ) as output_csvfile:
        csvreader = csv.DictReader(input_csvfile)
        fieldnames = csvreader.fieldnames
        csvwriter = csv.DictWriter(output_csvfile, fieldnames=fieldnames)

        csvwriter.writeheader()

        for row in csvreader:
            try:
                # Validate timestamp and write the row if valid
                datetime.datetime.strptime(row["date"], "%Y-%m-%d %H:%M:%S")
                csvwriter.writerow(row)
            except:
                continue


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Clear CSV file.")
    parser.add_argument("input_file_path", type=str, help="Input file path")
    parser.add_argument("output_file_path", type=str, help="Output file path")
    args = parser.parse_args()

    clear_csv(args.input_file_path, args.output_file_path)
