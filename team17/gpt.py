import aiohttp
import asyncio
import json
import os
import argparse
import datetime
import csv
import re
import logging
import pandas as pd
import typing
import numpy as np

from dataclasses import dataclass, asdict
from enum import Enum
from asyncio import Queue, Lock
from datetime import datetime, timedelta
from collections import defaultdict


@dataclass
class TwitterPost:
    text: str
    user: str
    user_location: str
    followers_count: int
    timestamp: datetime


@dataclass
class Task:
    posts: typing.List[TwitterPost]
    prices: typing.List[float]
    next_price: float


class GptPrediction(Enum):
    Up = 1
    Neutral = 2
    Down = 3


@dataclass
class Result:
    prediction: GptPrediction
    last_price: float
    next_price: float


async def fetch_gpt_response(api_key: str, task: Task):
    url = "https://api.openai.com/v1/chat/completions"
    headers = {"Content-Type": "application/json", "Authorization": f"Bearer {api_key}"}

    posts = []
    for post in task.posts:
        posts.append(
            f"user: '{post.user}', user_location: '{post.user_location}', followers: {post.followers_count}, text: {post.text}"
        )
    posts = "\n\n".join(posts)

    prompt = f"""
        Given past 30 min of prices and twitter posts,
        please assert sentiment and predict most likely category (identified by the [\d] number)
            [1] price goes up 0.5% or more,
            [2] stays within [-0.5%, +0.5%],
            [3] price goes down 0.5% or more
        Reply your prediction ONLY in format: "[{{NUMBER}}]
        POSTS:
        {posts}
        Past 30 minutes of prices (1min granularity, oldest to newest)
        {task.prices}
        "
    """
    payload = {
        "model": "gpt-3.5-turbo",
        "messages": [
            {
                "role": "system",
                "content": "You are very precise sentiment classification engine which produces machine-precision sentiment of a tweet",
            },
            {"role": "user", "content": prompt},
        ],
    }

    async with aiohttp.ClientSession() as session:
        async with session.post(url, headers=headers, json=payload) as resp:
            resp = await resp.json()
            prediction = resp["choices"][0]["message"]["content"]
            match = re.search(r"\[(\d+)\]", prediction)
            if match:
                return Result(
                    prediction=GptPrediction(int(match.group(1))),
                    last_price=task.prices[-1],
                    next_price=task.next_price,
                )
            else:
                return None


def read_csv(file_path):
    with open(file_path, "r", newline="", encoding="utf-8") as csvfile:
        csvreader = csv.DictReader(csvfile)

        for row in csvreader:
            timestamp = datetime.strptime(row["date"], "%Y-%m-%d %H:%M:%S")
            twitter_post = TwitterPost(
                text=row["text"],
                user=row["user_name"],
                user_location=row["user_location"],
                followers_count=int(float(row["user_followers"])),
                timestamp=timestamp,
            )
            yield twitter_post


def filter_and_group_posts(posts):
    start_time = datetime(2021, 7, 20)
    target_date = start_time.date()

    grouped_posts = defaultdict(list)

    for post in posts:
        if post.timestamp.date() != target_date:
            continue
        minute_of_day = int((post.timestamp - start_time).total_seconds() / 60)
        t = start_time + timedelta(minutes=minute_of_day)
        grouped_posts[t].append(post)

    for key, group in grouped_posts.items():
        group.sort(key=lambda x: x.followers_count, reverse=True)
        grouped_posts[key] = group[:5]
    return grouped_posts


async def worker(api_key, queue, results, lock):
    while True:
        post = await queue.get()
        try:
            result = await fetch_gpt_response(api_key, post)
            print(result)
            if result is not None:
                async with lock:
                    results.append(result)
                    write_results_to_csv(results, "output.csv")
        except Exception as e:
            logging.error(f"failed to process the gpt response: {e}")
        queue.task_done()


BTC_DATASET_PATH = "../dataset/binance_1m_prices.csv"


def load_btc_prices():
    btc_df = pd.read_csv(BTC_DATASET_PATH)
    btc_df["time"] = pd.to_datetime(btc_df["time"])
    btc_df = btc_df.set_index("time")
    return btc_df


def get_prices(btc_df, t: datetime):
    t = pd.to_datetime(t)
    try:
        return list(btc_df.loc[t - pd.Timedelta(minutes=30) : t, "close"].values)
    except:
        return None


def get_next_price(btc_df, t: datetime):
    t = pd.to_datetime(t)
    try:
        return float(btc_df.loc[t + pd.Timedelta(minutes=1), "close"])
    except:
        # Find the closest next price in case of non-unique or missing timestamp
        return None


def write_results_to_csv(results: list, filename: str):
    with open(filename, "w", newline="") as csvfile:
        fieldnames = ["prediction", "last_price", "next_price"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for result in results:
            row = asdict(result)
            row["prediction"] = row["prediction"].name  # Convert enum to its name
            writer.writerow(row)


async def main(api_key):
    btc_df = load_btc_prices()

    file_path = "../dataset/Bitcoin_tweets_clean.csv"
    queue = Queue(40)
    results = []
    lock = Lock()

    # Spawn 20 workers
    workers = [
        asyncio.create_task(worker(api_key, queue, results, lock)) for _ in range(20)
    ]

    groups = filter_and_group_posts(read_csv(file_path))

    for i, (timestamp, group) in enumerate(groups.items()):
        task = Task(
            posts=group,
            prices=get_prices(btc_df, timestamp),
            next_price=get_next_price(btc_df, timestamp),
        )
        await queue.put(task)

    # Wait for all tasks in the queue to be processed
    await queue.join()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Fetch GPT response.")
    parser.add_argument(
        "--api_key",
        type=str,
        default=os.environ.get("GPT_API_KEY"),
        help="API key for GPT",
    )
    args = parser.parse_args()

    if args.api_key is None:
        print("API key must be provided via --api_key or GPT_API_KEY env variable.")
        exit(1)

    asyncio.run(main(args.api_key))
