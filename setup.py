"""Package setup."""
import pathlib

from setuptools import find_packages, setup

VERSION = "0.0.1-alpha.1"

REPO_ROOT = pathlib.Path(__file__).parent

with open(REPO_ROOT / "README.md", encoding="utf-8") as f:
    README = f.read()

setup(
    name="team17",
    version=VERSION,
    long_description=README,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    python_requires=">=3.10",
)

