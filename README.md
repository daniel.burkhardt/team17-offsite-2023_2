# Team 17

## Data

Download tweets zip from: https://drive.google.com/drive/folders/1W4pph4tJ29MYbH6PJv0AylFCJp1_oCL5

## Python env setup

Run the following:
```bash
python -m venv .venv
source .venv/bin/activate
```

Now you're in a local (encapsulated) python environment (so you won't f your system python
installation).

Run the following to install the projects dependencies (make sure you've done the step above before
in the same terminal session):
```bash
pip install -r requirments.txt
pip install -e .
```

## Development

After you've done `Python env setup` above. Start a jupyter notebook server with:
```bash
juptyer-notebook
```

Make a dir in `notebooks` with your name so we keep our trash separate. You can copy
`notebooks/dan/example_notebook.ipynb` for a starting point.
